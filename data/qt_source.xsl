<?xml version="1.0" encoding="utf-8"?>

<!-- This file is part of SConsX.                                            -->
<!--                                                                         -->
<!-- SConsX is an extension of the SCons - a software construction tool.     -->
<!-- Copyright (C) 2007-2017 Yanis Kurganov <yanis.kurganov@gmail.com>       -->
<!--                                                                         -->
<!-- Permission is hereby granted, free of charge, to any person obtaining   -->
<!-- a copy of this software and associated documentation files (the         -->
<!-- "Software"), to deal in the Software without restriction, including     -->
<!-- without limitation the rights to use, copy, modify, merge, publish,     -->
<!-- distribute, sublicense, and/or sell copies of the Software, and to      -->
<!-- permit persons to whom the Software is furnished to do so, subject to   -->
<!-- the following conditions:                                               -->
<!--                                                                         -->
<!-- The above copyright notice and this permission notice shall be included -->
<!-- in all copies or substantial portions of the Software.                  -->
<!--                                                                         -->
<!-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY               -->
<!-- KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE              -->
<!-- WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND     -->
<!-- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE  -->
<!-- LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION  -->
<!-- OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION   -->
<!-- WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.         -->

<xsl:stylesheet version="1.0" xml:lang="en"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema"
    xmlns:sconsx="https://gitlab.com/ykurganov/sconsx"
    extension-element-prefixes="sconsx">

<xsl:include href="shared.xsl"/>

<xsl:output method="text" encoding="utf-8" indent="no"/>
<xsl:strip-space elements="*"/>

<xsl:param name="source_filename">Unknown</xsl:param>
<xsl:param name="style_filename">Unknown</xsl:param>
<xsl:param name="creator">Unknown</xsl:param>

<xsl:template match="xsd:schema">
<xsl:value-of select="sconsx:header($source_filename, $style_filename, @version, $creator)"/>
#include "settings.hpp"
<xsl:apply-templates select="xsd:element" mode="namespace"/>
<xsl:text>
</xsl:text>
</xsl:template>

<xsl:template match="xsd:element[@id='namespace']" mode="namespace">
namespace <xsl:value-of select="@name"/>
{
<xsl:apply-templates select="xsd:element" mode="load"/>
<xsl:apply-templates select="xsd:element" mode="save"/>
<xsl:apply-templates select="xsd:element" mode="eq"/>
<xsl:apply-templates select="xsd:element" mode="ne"/>
<xsl:apply-templates select="xsd:element" mode="definition"/>
} // namespace <xsl:value-of select="@name"/>
</xsl:template>

<xsl:template match="xsd:element[@id='class']" mode="load">
void <xsl:value-of select="@name"/>::Load(QSettings&#38; settings)
{
<xsl:apply-templates mode="do_load"/>
}
</xsl:template>

<xsl:template match="xsd:element[@id='class']" mode="save">
void <xsl:value-of select="@name"/>::Save(QSettings&#38; settings) const
{
<xsl:apply-templates mode="do_save"/>
}
</xsl:template>

<xsl:template match="xsd:element[@id='class']" mode="eq">
bool <xsl:value-of select="@name"/>::operator ==(const <xsl:value-of select="@name"/>&#38; other) const
{
return
<xsl:apply-templates mode="do_eq"/>
true;
}
</xsl:template>

<xsl:template match="xsd:element[@id='class']" mode="ne">
bool <xsl:value-of select="@name"/>::operator !=(const <xsl:value-of select="@name"/>&#38; other) const
{
return
<xsl:apply-templates mode="do_ne"/>
false;
}
</xsl:template>

<xsl:template match="xsd:element[@id='class']" mode="definition">
<xsl:apply-templates mode="do_definition"/>
</xsl:template>

<xsl:template match="xsd:element[@type]" mode="do_load">
<xsl:for-each select="ancestor::xsd:element[not(@id)]"><xsl:value-of select="@name"/><xsl:text>.</xsl:text></xsl:for-each>
<xsl:text>m_</xsl:text><xsl:value-of select="@name"/>
<xsl:text> = settings.value("</xsl:text>
<xsl:value-of select="@name"/>", <xsl:choose><xsl:when test="@default"><xsl:value-of select="@default"/></xsl:when>
<xsl:otherwise>QVariant(QVariant::nameToType("<xsl:value-of select="@type"/>"))</xsl:otherwise>
</xsl:choose>);
</xsl:template>

<xsl:template match="xsd:element[@type]" mode="do_save">
if (!<xsl:for-each select="ancestor::xsd:element[not(@id)]"><xsl:value-of select="@name"/><xsl:text>.</xsl:text></xsl:for-each>
<xsl:text>m_</xsl:text><xsl:value-of select="@name"/>.isNull())
{
settings.setValue("<xsl:value-of select="@name"/><xsl:text>", </xsl:text>
<xsl:for-each select="ancestor::xsd:element[not(@id)]"><xsl:value-of select="@name"/><xsl:text>.</xsl:text></xsl:for-each>
<xsl:text>m_</xsl:text><xsl:value-of select="@name"/>);
}
</xsl:template>

<xsl:template match="xsd:element[@type]" mode="do_eq">
<xsl:for-each select="ancestor::xsd:element[not(@id)]"><xsl:value-of select="@name"/><xsl:text>.</xsl:text></xsl:for-each>
<xsl:text>m_</xsl:text><xsl:value-of select="@name"/><xsl:text> == other.</xsl:text>
<xsl:for-each select="ancestor::xsd:element[not(@id)]"><xsl:value-of select="@name"/><xsl:text>.</xsl:text></xsl:for-each>
<xsl:text>m_</xsl:text><xsl:value-of select="@name"/> &#38;&#38;
</xsl:template>

<xsl:template match="xsd:element[@type]" mode="do_ne">
<xsl:for-each select="ancestor::xsd:element[not(@id)]"><xsl:value-of select="@name"/><xsl:text>.</xsl:text></xsl:for-each>
<xsl:text>m_</xsl:text><xsl:value-of select="@name"/><xsl:text> != other.</xsl:text>
<xsl:for-each select="ancestor::xsd:element[not(@id)]"><xsl:value-of select="@name"/><xsl:text>.</xsl:text></xsl:for-each>
<xsl:text>m_</xsl:text><xsl:value-of select="@name"/> ||
</xsl:template>

<xsl:template match="xsd:element[@type]" mode="do_definition">
void <xsl:value-of select="ancestor::xsd:element[@id='class']/@name"/>::<xsl:for-each select="ancestor::xsd:element[not(@id)]">Group<xsl:value-of select="@name"/>
<xsl:text>::</xsl:text></xsl:for-each>Set<xsl:value-of select="@name"/><xsl:text>(const </xsl:text><xsl:value-of select="@type"/>&#38; value)
{
m_<xsl:value-of select="@name"/>.setValue&#60;<xsl:value-of select="@type"/>&#62;(value);
}

<xsl:value-of select="@type"/><xsl:text>&#32;</xsl:text><xsl:value-of select="ancestor::xsd:element[@id='class']/@name"/><xsl:text>::</xsl:text>
<xsl:for-each select="ancestor::xsd:element[not(@id)]">Group<xsl:value-of select="@name"/>::</xsl:for-each><xsl:value-of select="@name"/>() const
{
return m_<xsl:value-of select="@name"/>.value&#60;<xsl:value-of select="@type"/>&#62;();
}
</xsl:template>

<xsl:template match="xsd:element" mode="do_load">
settings.beginGroup("<xsl:value-of select="@name"/>");
<xsl:apply-templates mode="do_load"/>
settings.endGroup();
</xsl:template>

<xsl:template match="xsd:element" mode="do_save">
settings.beginGroup("<xsl:value-of select="@name"/>");
<xsl:apply-templates mode="do_save"/>
settings.endGroup();
</xsl:template>

<xsl:template match="xsd:element" mode="do_eq">
<xsl:apply-templates mode="do_eq"/>
</xsl:template>

<xsl:template match="xsd:element" mode="do_ne">
<xsl:apply-templates mode="do_ne"/>
</xsl:template>

<xsl:template match="xsd:element" mode="do_definition">
<xsl:apply-templates mode="do_definition"/>
</xsl:template>

</xsl:stylesheet>
