<?xml version="1.0" encoding="utf-8"?>

<!-- This file is part of SConsX.                                            -->
<!--                                                                         -->
<!-- SConsX is an extension of the SCons - a software construction tool.     -->
<!-- Copyright (C) 2007-2017 Yanis Kurganov <yanis.kurganov@gmail.com>       -->
<!--                                                                         -->
<!-- Permission is hereby granted, free of charge, to any person obtaining   -->
<!-- a copy of this software and associated documentation files (the         -->
<!-- "Software"), to deal in the Software without restriction, including     -->
<!-- without limitation the rights to use, copy, modify, merge, publish,     -->
<!-- distribute, sublicense, and/or sell copies of the Software, and to      -->
<!-- permit persons to whom the Software is furnished to do so, subject to   -->
<!-- the following conditions:                                               -->
<!--                                                                         -->
<!-- The above copyright notice and this permission notice shall be included -->
<!-- in all copies or substantial portions of the Software.                  -->
<!--                                                                         -->
<!-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY               -->
<!-- KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE              -->
<!-- WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND     -->
<!-- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE  -->
<!-- LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION  -->
<!-- OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION   -->
<!-- WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.         -->

<xsl:stylesheet version="1.0" xml:lang="en"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema"
    xmlns:sconsx="https://gitlab.com/ykurganov/sconsx"
    extension-element-prefixes="sconsx">

<xsl:include href="shared.xsl"/>

<xsl:output method="text" encoding="utf-8" indent="no"/>
<xsl:strip-space elements="*"/>

<xsl:param name="source_filename">Unknown</xsl:param>
<xsl:param name="style_filename">Unknown</xsl:param>
<xsl:param name="creator">Unknown</xsl:param>

<xsl:template match="xsd:schema">
<xsl:value-of select="sconsx:header($source_filename, $style_filename, @version, $creator)"/>
#ifndef HEADER_SCONSX_SETTINGS_HPP
#define HEADER_SCONSX_SETTINGS_HPP

#include &#60;QSharedPointer&#62;
#include &#60;QSettings&#62;
<xsl:apply-templates select="xsd:element" mode="namespace"/>

#endif /* HEADER_SCONSX_SETTINGS_HPP */
</xsl:template>

<xsl:template match="xsd:element[@id='namespace']" mode="namespace">
namespace <xsl:value-of select="@name"/>
{
<xsl:apply-templates select="xsd:element" mode="class"/>
} // namespace <xsl:value-of select="@name"/>
</xsl:template>

<xsl:template match="xsd:element[@id='class']" mode="class">
class <xsl:value-of select="@name"/>
{
public:
void Load(QSettings&#38;);
void Save(QSettings&#38;) const;

bool operator ==(const Settings&#38;) const;
bool operator !=(const Settings&#38;) const;
<xsl:apply-templates mode="declaration"/>
}; // class <xsl:value-of select="@name"/>

typedef QSharedPointer&#60;<xsl:value-of select="@name"/>&#62; <xsl:value-of select="@name"/>Ptr;
</xsl:template>

<xsl:template match="xsd:element[@type]" mode="declaration">
public:
void Set<xsl:value-of select="@name"/>(const <xsl:value-of select="@type"/>&#38;);
<xsl:value-of select="@type"/><xsl:text>&#32;</xsl:text><xsl:value-of select="@name"/>() const;
private:
QVariant m_<xsl:value-of select="@name"/>;
</xsl:template>

<xsl:template match="xsd:element" mode="declaration">
private:
class Group<xsl:value-of select="@name"/>
{
friend class Settings;
<xsl:apply-templates mode="declaration"/>
}; // class Group<xsl:value-of select="@name"/>
public:
Group<xsl:value-of select="@name"/><xsl:text>&#32;</xsl:text><xsl:value-of select="@name"/>;
</xsl:template>

</xsl:stylesheet>
