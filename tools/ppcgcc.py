# -*- coding: utf-8 -*-

# This file is part of SConsX.
#
# SConsX is an extension of the SCons - a software construction tool.
# Copyright (C) 2007-2017 Yanis Kurganov <yanis.kurganov@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
# KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import SCons

def detect(env):
    key_name = "powerpc-linux-gnu"
    key_program = key_name + "-gcc"
    key_program = env.WhereIs(key_program) or SCons.Util.WhereIs(key_program)
    return key_name if key_program is not None else None

def generate(env):
    for tool in ("gnulink", "gcc", "g++", "gas", "ar"):
        env.Tool(tool)

    base_name = detect(env) or ""

    env["CC"] = base_name + "-gcc"
    env["CXX"] = base_name + "-g++"
    env["AS"] = base_name + "-as"
    env["RC"] = base_name + "-windres"
    env["AR"] = base_name + "-ar"
    env["RANLIB"] = base_name + "-ranlib"

def exists(env):
    return detect(env)
