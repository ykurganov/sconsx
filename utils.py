# -*- coding: utf-8 -*-

# This file is part of SConsX.
#
# SConsX is an extension of the SCons - a software construction tool.
# Copyright (C) 2007-2017 Yanis Kurganov <yanis.kurganov@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
# KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import os, logging, itertools, collections, SCons

def interrupt(env, message):
    logging.critical(env.subst(message))
    raise SCons.Errors.StopError("A critical error has occurred and SCons can not continue building.")

def log_debug(env, message):
    logging.debug(env.subst(message))

def log_information(env, message):
    logging.info(env.subst(message))

def log_warning(env, message):
    logging.warning(env.subst(message))

def log_error(env, message):
    logging.error(env.subst(message))

def log_critical(env, message):
    logging.critical(env.subst(message))

def traverse_files(node, patterns, env):
    return (file for pattern in patterns for file in node.glob(pattern) if isinstance(file, SCons.Node.FS.File))

def traverse_dirs(node, patterns, env):
    return (file for dir in node.glob("*") if isinstance(dir, SCons.Node.FS.Dir) for file in traverse_all(dir, patterns, env))

def traverse_all(node, patterns, env):
    return itertools.chain(traverse_files(node, patterns, env), traverse_dirs(node, patterns, env))

def get_nodes(paths, patterns, env):
    return (file for path in paths for file in traverse_all(env.Dir(path), patterns, env))

bit_suffix_map = [
    ("SCONSX_OPENGL_BIT", "opengl"),
    ("SCONSX_X11_BIT", "x11"),
    ("SCONSX_LINUX_BIT", "linux"),
    ("SCONSX_POSIX_BIT", "posix"),
    ("SCONSX_WINDOWS_BIT", "windows"),
    ("SCONSX_GNUC_BIT", "gnuc"),
    ("SCONSX_MINGW_BIT", "mingw"),
    ("SCONSX_GCC_BIT", "gcc"),
    ("SCONSX_GENERIC_BIT", "generic"),
]

def filter_sources(env, nodes):
    nodes = nodes[:]
    get_name = lambda node: os.path.splitext(node.name)[0].split('-')[0]
    get_key = lambda node: os.path.splitext(node.name)[0].split('-')[1]
    key_nodes = (node for node in nodes if -1 != node.name.find('-'))
    cache = dict()
    for node in key_nodes:
        values = cache.get(get_name(node), [])
        values.append(node)
        cache[get_name(node)] = values
    for name, values in cache.iteritems():
        def select():
            for suffix in (suffix for bit, suffix in bit_suffix_map if env.has_key(bit) and env[bit] is True):
                for node in (node for node in values if get_key(node) == suffix):
                    return node
            return None
        node = select()
        if node:
            log_debug(env, "utils: using '{0}' for '{1}' from [{2}]".format(node.name, name, ", ".join(node.name for node in values)))
            values.remove(node)
        else:
            log_debug(env, "utils: could not deduct platform-depended file for '{0}' from [{1}]".format(name, ", ".join(node.name for node in values)))
        for node in values:
            nodes.remove(node)
    return nodes

def unfold(node):
    while issubclass(type(node), (list, collections.UserList)):
        node = node[0]
    return node

def adjustixes(node, prefix, suffix):
    return SCons.Util.adjustixes(os.path.splitext(unfold(node).name)[0], prefix, suffix)

def root(node):
    return os.path.splitext(unfold(node).name)[0]

def ext(node):
    return os.path.splitext(unfold(node).name)[1]
