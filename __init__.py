# -*- coding: utf-8 -*-

# This file is part of SConsX
#
# SConsX is an extension of the SCons - a software construction tool
# Copyright (C) 2007-2017 Yanis Kurganov <yanis.kurganov@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
# KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import utils, hosts, gls, tools.qt, tools.settings, os, logging, ConfigParser, SCons

def create_env(project_name):
    defenv = SCons.Defaults.DefaultEnvironment()
    topdir = defenv.Dir('#').path

    config = ConfigParser.SafeConfigParser()
    config.read(os.path.join(topdir, cfg) for cfg in (project_name + ".cfg", project_name + ".cfg.user"))

    get_value = lambda sec, opt, default=None: config.get(sec, opt) if config.has_option(sec, opt) else default
    get_list = lambda sec, opt, default="": [value for value in get_value(sec, opt, default).split(';') if len(value) > 0]

    host = SCons.Script.ARGUMENTS.get("SCONSX_HOST", get_value("SCONSX", "HOST", hosts.defaults[defenv["PLATFORM"]]))

    logging_levels = {
        "debug": logging.DEBUG,
        "info": logging.INFO,
        "warning": logging.WARNING,
        "error": logging.ERROR,
        "critical": logging.CRITICAL,
    }

    variables = SCons.Variables.Variables(args=SCons.Script.ARGUMENTS)

    variables.Add(
        "SCONSX_VERSION_MAJOR",
        "Software major version",
        get_value("SCONSX", "VERSION_MAJOR")
    )

    variables.Add(
        "SCONSX_VERSION_MINOR",
        "Software minor version",
        get_value("SCONSX", "VERSION_MINOR")
    )

    variables.Add(
        SCons.Variables.EnumVariable(
            "SCONSX_HOST",
            "Build for HOST",
            host,
            hosts.hosts.keys()
        )
    )

    variables.Add(
        SCons.Variables.BoolVariable(
            "SCONSX_RELEASE",
            "Build the project in release mode",
            get_value("SCONSX", "RELEASE", "yes")
        )
    )

    variables.Add(
        SCons.Variables.EnumVariable(
            "SCONSX_LOGGING_LEVEL",
            "Select logging level",
            get_value("SCONSX", "LOGGING_LEVEL", "info"),
            logging_levels.keys()
        )
    )

    variables.Add(
        SCons.Variables.ListVariable(
            "SCONSX_MODULES",
            "SCONSX modules",
            get_list("SCONSX", "MODULES"),
            gls.modules.keys()
        )
    )

    variables.Add(
        SCons.Variables.BoolVariable(
            "SCONSX_QT_USE_ONLY_RELEASE_LIBS",
            "Use only release QT libraries",
            get_value(host, "QT_USE_ONLY_RELEASE_LIBS", get_value("QT", "USE_ONLY_RELEASE_LIBS", "no"))
        )
    )

    variables.Add(
        SCons.Variables.PathVariable(
            "SCONSX_QT_PATH",
            "Qt path",
            get_value(host, "QT_PATH", get_value("QT", "PATH")),
            SCons.Variables.PathVariable.PathIsDir
        )
    )

    variables.Add(
        SCons.Variables.ListVariable(
            "SCONSX_QT_MODULES",
            "Qt modules",
            get_list("QT", "MODULES"),
            tools.qt.modules.keys()
        )
    )

    variables.Add(
        SCons.Variables.ListVariable(
            "SCONSX_QT_FEATURES",
            "Qt features",
            get_list("QT", "FEATURES"),
            tools.qt.features.keys()
        )
    )

    variables.Add(
        "SCONSX_INNO_NAME",
        "The name of the application being installed",
        get_value("INNO", "NAME")
    )

    variables.Add(
        "SCONSX_INNO_PUBLISHER",
        "This string is displayed on the Support dialog of the Add/Remove Programs Control Panel applet",
        get_value("INNO", "PUBLISHER")
    )

    variables.Add(
        "SCONSX_INNO_URL",
        "A link to the specified URL is displayed on the Support dialog of the Add/Remove Programs Control Panel applet",
        get_value("INNO", "URL")
    )

    variables.Add(
        "SCONSX_INNO_MAIN_EXE_NAME",
        "The name of the main executable file",
        get_value("INNO", "MAIN_EXE_NAME")
    )

    variables.Add(
        SCons.Variables.EnumVariable(
            "SCONSX_SETTINGS_STYLESHEET",
            "Predefined stylesheet",
            get_value("SETTINGS", "STYLESHEET"),
            tools.settings.stylesheets.keys()
        )
    )

    variables.Add(
        SCons.Variables.PathVariable(
            "SCONSX_SETTINGS_HEADER_STYLESHEET",
            "Stylesheet for header",
            get_value("SETTINGS", "HEADER_STYLESHEET"),
            SCons.Variables.PathVariable.PathIsFile
        )
    )

    variables.Add(
        SCons.Variables.PathVariable(
            "SCONSX_SETTINGS_SOURCE_STYLESHEET",
            "Stylesheet for source",
            get_value("SETTINGS", "SOURCE_STYLESHEET"),
            SCons.Variables.PathVariable.PathIsFile
        )
    )

    variables.Add(
        SCons.Variables.BoolVariable(
            "SCONSX_SIGNTOOL_SIGN",
            "Use sign tool or not",
            get_value(host, "SIGNTOOL_SIGN", get_value("SIGNTOOL", "SIGN", "yes"))
        )
    )

    variables.Add(
        "SCONSX_SIGNTOOL_DESCRIPTION",
        "Sign tool description",
        get_value("SIGNTOOL", "DESCRIPTION")
    )

    variables.Add(
        "SCONSX_SIGNTOOL_TIMESTAMP_SERVER",
        "Sign tool time stamp server",
        get_value("SIGNTOOL", "TIMESTAMP_SERVER")
    )

    variables.Add(
        SCons.Variables.PathVariable(
            "SCONSX_SIGNTOOL_CERTIFICATE",
            "Sign tool certificate",
            get_value("SIGNTOOL", "CERTIFICATE"),
            SCons.Variables.PathVariable.PathIsFile
        )
    )

    variables.Add(
        "SCONSX_SIGNTOOL_PASSWORD",
        "Sign tool password",
        get_value("SIGNTOOL", "PASSWORD")
    )

    env = SCons.Environment.Environment(
        variables=variables,
        tools=[],
        toolpath=[os.path.join("#", "sconsx", "tools")]
    )

    env.AddMethod(lambda env, **args: env.Append(**{name:args[name] for name in args if args[name] is not None}), "AppendExist")

    logging.basicConfig(
        level=logging_levels[env["SCONSX_LOGGING_LEVEL"]],
        format="%(levelname)s: %(message)s",
        datefmt="%a, %d %b %Y %H:%M:%S",
    )

    env["SCONSX_VERSION_BUILD"] = 0
    env["SCONSX_VERSION_REVISION"] = 0

    env["SCONSX_VERSION"] = "${SCONSX_VERSION_MAJOR}.${SCONSX_VERSION_MINOR}.${SCONSX_VERSION_BUILD}.${SCONSX_VERSION_REVISION}"
    env["SCONSX_VERSION_STR"] = "\\\"$SCONSX_VERSION\\\""

    env["SCONSX_ARCHITECTURE_STR"] = "\\\"$SCONSX_ARCHITECTURE\\\""

    env["SCONSX_LINUX_BIT"] = False
    env["SCONSX_POSIX_BIT"] = False
    env["SCONSX_WINDOWS_BIT"] = False
    env["SCONSX_X11_BIT"] = False
    env["SCONSX_GNUC_BIT"] = False
    env["SCONSX_MINGW_BIT"] = False
    env["SCONSX_GCC_BIT"] = False
    env["SCONSX_OPENGL_BIT"] = False
    env["SCONSX_GENERIC_BIT"] = True

    env["SCONSX_BUILD_MODE"] = "release" if env["SCONSX_RELEASE"] else "debug"

    env["SCONSX_BUILD_PATH"] = os.path.join("#", "build")
    env["SCONSX_OBJECT_BUILD_PATH"] = os.path.join("$SCONSX_BUILD_PATH", "objects", "$SCONSX_HOST", "$SCONSX_BUILD_MODE")
    env["SCONSX_LIBRARY_BUILD_PATH"] = os.path.join("$SCONSX_BUILD_PATH", "libraries", "$SCONSX_HOST", "$SCONSX_BUILD_MODE")
    env["SCONSX_BINARY_BUILD_PATH"] = os.path.join("$SCONSX_BUILD_PATH", "binary", "$SCONSX_HOST", "$SCONSX_BUILD_MODE")
    env["SCONSX_DEPLOY_BUILD_PATH"] = os.path.join("$SCONSX_BUILD_PATH", "deploy")

    env["SCONSX_BUILD_DIR"] = env.Dir("$SCONSX_BUILD_PATH")
    env["SCONSX_OBJECT_BUILD_DIR"] = env.Dir("$SCONSX_OBJECT_BUILD_PATH")
    env["SCONSX_LIBRARY_BUILD_DIR"] = env.Dir("$SCONSX_LIBRARY_BUILD_PATH")
    env["SCONSX_BINARY_BUILD_DIR"] = env.Dir("$SCONSX_BINARY_BUILD_PATH")
    env["SCONSX_DEPLOY_BUILD_DIR"] = env.Dir("$SCONSX_DEPLOY_BUILD_PATH")

    env.AppendENVPath(
        "PATH", get_list("SCONSX", "BINARY_PATHS"),
    )

    env.AppendENVPath(
        "PATH", get_list(host, "BINARY_PATHS"),
    )

    env.AppendUnique(
        LIBPATH=["$SCONSX_LIBRARY_BUILD_PATH"],
        CPPDEFINES=[
            "SCONSX_VERSION_MAJOR=$SCONSX_VERSION_MAJOR",
            "SCONSX_VERSION_MINOR=$SCONSX_VERSION_MINOR",
            "SCONSX_VERSION_BUILD=$SCONSX_VERSION_BUILD",
            "SCONSX_VERSION_REVISION=$SCONSX_VERSION_REVISION",

            "SCONSX_VERSION=$SCONSX_VERSION",
            "SCONSX_VERSION_STR=$SCONSX_VERSION_STR",

            "SCONSX_ARCHITECTURE=$SCONSX_ARCHITECTURE",
            "SCONSX_ARCHITECTURE_STR=$SCONSX_ARCHITECTURE_STR",
        ],
    )

    env.AppendUnique(
        CPPPATH=get_list("SCONSX", "INCLUDE_PATHS"),
        LIBPATH=get_list("SCONSX", "LIBRARY_PATHS"),
        CPPDEFINES=get_list("SCONSX", "DEFINES"),
        LIBS=get_list("SCONSX", "LIBS"),
    )

    env.AppendUnique(
        CPPPATH=get_list(host, "INCLUDE_PATHS"),
        LIBPATH=get_list(host, "LIBRARY_PATHS"),
        CPPDEFINES=get_list(host, "DEFINES"),
        LIBS=get_list(host, "LIBS"),
    )

    env.AppendExist(
        SCONSX_QT_BIN_PATH=get_value(host, "QT_BIN_PATH"),
        SCONSX_QT_CPP_PATH=get_value(host, "QT_CPP_PATH"),
        SCONSX_QT_LIB_PATH=get_value(host, "QT_LIB_PATH"),
    )

    hosts.hosts[host].configure(env)

    utils.log_information(env, "building in $SCONSX_BUILD_MODE mode...")

    for module in env["SCONSX_MODULES"]:
        gls.modules[module](env)

    env.Help(variables.GenerateHelpText(env))

    return env
