# -*- coding: utf-8 -*-

# This file is part of SConsX.
#
# SConsX is an extension of the SCons - a software construction tool.
# Copyright (C) 2007-2017 Yanis Kurganov <yanis.kurganov@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
# KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import utils, os, datetime, hashlib, SCons

def get_paths(node):
    return [line for line in node.get_contents().splitlines() if len(line) > 0 and not line.startswith(';')]

def make_nodes(cache, env):
    return [env.File(os.path.join("$SCONSX_RES_START_PATH", path)) for path in get_paths(cache)]

def write_header(file, header):
    file.write(header % datetime.datetime.now().strftime("%d %b %Y %H:%M:%S"))

def emit(target, source, env):
    env["SCONSX_RES_START_PATH"] = source[0].dir.path

    resource_name = os.path.splitext(source[0].name)[0]
    excludes = get_paths(source[0])

    paths = sorted(path for path in (os.path.relpath(node.path, env["SCONSX_RES_START_PATH"]) for node in utils.get_nodes([source[0].dir], ["*"], env)) if not path in excludes)
    cache = env.File(os.path.join("$SCONSX_RES_BUILD_PATH", SCons.Util.adjustixes(resource_name, "", "${SCONSX_RES_CACHE_SUFFIX}")))

    # do not create a cache file if scons -c
    if not env.GetOption("clean") and (not os.path.exists(cache.abspath) or paths != get_paths(cache)):
        # note that SCons will create directories only before the builders
        if not os.path.exists(cache.dir.abspath):
            env.Execute(SCons.Defaults.Mkdir(cache.dir))
        utils.log_debug(env, "res: write '{0}'".format(cache.path))
        with open(cache.abspath, "wt") as file:
            write_header(file, cache_header)
            file.write('\n' + '\n'.join(paths) + '\n')

    env.ResHeader(os.path.join("$SCONSX_RES_BUILD_PATH", resource_name), cache)

    source = [env.ResSource(os.path.join("$SCONSX_RES_BUILD_PATH", resource_name), cache)]
    target = [os.path.join("$SCONSX_RES_OBJECT_BUILD_PATH", SCons.Util.adjustixes(resource_name, "${OBJPREFIX}", "${OBJSUFFIX}"))]

    # cache node is not a normal target, so mark it as cleanable
    env.Clean(target[0], cache)

    return target, source

def build_res_header(target, source, env):
    with open(target[0].abspath, "wt") as file:
        write_header(file, source_header)
        file.write(hpp_content)

def generate_resource_data(env, nodes, sizes, names):
    result = ""
    for node in nodes:
        name = hashlib.md5(os.path.relpath(node.path, env["SCONSX_RES_START_PATH"])).hexdigest()
        names.append(name)
        contents = node.get_contents()
        sizes.append(len(contents))
        lines = []
        while len(contents) > 0:
            line, contents = contents[:15], contents[15:]
            lines.append(",".join(hex(ord(c)) for c in line))
        result += resource_data_template.format(name, ",\n".join(lines))
    return result

def build_res_source(target, source, env):
    nodes, sizes, names = make_nodes(source[0], env), [], []
    with open(target[0].abspath, "wt") as file:
        write_header(file, source_header)
        file.write(cpp_content.format(
            SCons.Util.adjustixes(os.path.splitext(source[0].name)[0], env.subst("$SCONSX_RES_PREFIX"), env.subst("$SCONSX_RES_HEADER_SUFFIX")),
            generate_resource_data(env, nodes, sizes, names),
            len(nodes),
            "\n".join("{0},".format(size) for size in sizes),
            "\n".join('"{0}",'.format(os.path.relpath(node.path, env["SCONSX_RES_START_PATH"]).replace("\\", "/")) for node in nodes),
            "\n".join(["ce_resource_data_{0},".format(str(name)) for name in names])
        ))

def generate(env):
    env.SetDefault(
        SCONSX_RES_START_PATH="",
        SCONSX_RES_BUILD_PATH=os.path.join("$SCONSX_BUILD_PATH", "resources"),
        SCONSX_RES_OBJECT_BUILD_PATH="$SCONSX_OBJECT_BUILD_PATH",

        SCONSX_RES_PREFIX="",
        SCONSX_RES_HEADER_SUFFIX=".hpp",
        SCONSX_RES_SOURCE_SUFFIX=".cpp",
        SCONSX_RES_CACHE_SUFFIX=".cache",
        SCONSX_RES_SRC_SUFFIX=".res",
    )

    env.AppendUnique(
        CPPPATH=["$SCONSX_RES_BUILD_PATH"],
    )

    env.Append(
        SCANNERS=SCons.Scanner.Scanner(
            function=lambda node, env, *args: make_nodes(node, env),
            skeys="$SCONSX_RES_CACHE_SUFFIX",
            recursive=False,
        ),
        BUILDERS={
            "ResHeader": SCons.Builder.Builder(
                action=build_res_header,
                prefix="$SCONSX_RES_PREFIX",
                suffix="$SCONSX_RES_HEADER_SUFFIX",
                src_suffix="$SCONSX_RES_CACHE_SUFFIX",
                single_source=True,
            ),
            "ResSource": SCons.Builder.Builder(
                action=build_res_source,
                prefix="$SCONSX_RES_PREFIX",
                suffix="$SCONSX_RES_SOURCE_SUFFIX",
                src_suffix="$SCONSX_RES_CACHE_SUFFIX",
                single_source=True,
            ),
        },
    )

    static_obj_builder, shared_obj_builder = SCons.Tool.createObjBuilders(env)
    static_obj_builder.add_action("$SCONSX_RES_SRC_SUFFIX", SCons.Defaults.CXXAction)
    shared_obj_builder.add_action("$SCONSX_RES_SRC_SUFFIX", SCons.Defaults.CXXAction)
    static_obj_builder.add_emitter("$SCONSX_RES_SRC_SUFFIX", emit)
    shared_obj_builder.add_emitter("$SCONSX_RES_SRC_SUFFIX", emit)

def exists(env):
    return True

cache_header = \
"""; Resource cache
;
; Created: %s
;      by: SConsX build system
;
; WARNING! All changes made in this file will be lost!
"""

source_header = \
"""/*
 * Resource data
 *
 * Created: %s
 *      by: SConsX build system
 *
 * WARNING! All changes made in this file will be lost!
*/
"""

hpp_content = \
"""
#ifndef SCONSX_RESOURCE_HPP
#define SCONSX_RESOURCE_HPP

#include <cstddef>

namespace cursedearth
{

extern const size_t CE_RESOURCE_DATA_COUNT;

extern const size_t ce_resource_data_sizes[];
extern const char* ce_resource_data_paths[];

extern const unsigned char* ce_resource_data[];

}

#endif
"""

cpp_content = \
"""
#include "{0}"

namespace
{{
{1}
}} // namespace

namespace cursedearth
{{

const size_t CE_RESOURCE_DATA_COUNT = {2};

const size_t ce_resource_data_sizes[] = {{
{3}
}};

const char* ce_resource_data_paths[] = {{
{4}
}};

const unsigned char* ce_resource_data[] = {{
{5}
}};

}} // namespace cursedearth
"""

resource_data_template = \
"""
const unsigned char ce_resource_data_{0}[] = {{
{1}
}};
"""
