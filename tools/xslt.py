# -*- coding: utf-8 -*-

# This file is part of SConsX.
#
# SConsX is an extension of the SCons - a software construction tool.
# Copyright (C) 2007-2017 Yanis Kurganov <yanis.kurganov@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
# KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import os, sys, re, lxml.etree as etree, SCons

xsl_include_re = re.compile(r"""
    <\s*\S+:                             # <xsl:
    (?:include|import)\s+                # include
    href\s*=\s*                          # href =
    ['"](\S+)['"]\s*                     # "file.xsl"
    (?:/>                                # />
    |                                    # or may be
    >.*?</\S+:(?:include|import)\s*>)    # >anything</xsl:include>
    """, re.VERBOSE | re.MULTILINE | re.DOTALL)

def transform(target, source, env):
    source_filename = source[0].path
    style_filename = source[1].path
    target_filename = target[0].path

    style = etree.XSLT(etree.parse(style_filename))
    result = style(etree.parse(source_filename),
        source_filename="'{0}'".format(os.path.basename(source_filename)),
        style_filename="'{0}'".format(os.path.basename(style_filename)),
        creator="'{0} {1}'".format("Python version", sys.version.replace('\n', '')),
    )
    result.write_output(target_filename)

def generate(env):
    env.SetDefault(
        SCONSX_XSLT_PREFIX="",
        SCONSX_XSLT_SUFFIX="",
        SCONSX_XSLT_SRC_SUFFIX="",
    )

    env.Append(
        BUILDERS={
            "Xslt": SCons.Builder.Builder(
                action=transform,
                source_scanner=SCons.Scanner.Scanner(
                    function=lambda node, *args: xsl_include_re.findall(node.get_text_contents()),
                    skeys="$SCONSX_XSLT_SRC_SUFFIX",
                    recursive=True,
                ),
                prefix="$SCONSX_XSLT_PREFIX",
                suffix="$SCONSX_XSLT_SUFFIX",
                src_suffix="$SCONSX_XSLT_SRC_SUFFIX",
                multi=True,
            ),
        },
    )

def exists(env):
    return True
