# -*- coding: utf-8 -*-

# This file is part of SConsX.
#
# SConsX is an extension of the SCons - a software construction tool.
# Copyright (C) 2007-2017 Yanis Kurganov <yanis.kurganov@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
# KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import SCons

def generate(env):
    env.SetDefault(
        SCONSX_CPP="cpp",
        SCONSX_CPP_FLAGS="$_CPPDEFFLAGS $_CPPINCFLAGS",

        SCONSX_CPP_COM="$SCONSX_CPP $SCONSX_CPP_FLAGS -o $TARGET $SOURCE",
        SCONSX_CPP_COM_STR="",

        SCONSX_CPP_PREFIX="",
        SCONSX_CPP_SUFFIX="",
        SCONSX_CPP_SRC_SUFFIX="",
    )

    env.Append(
        BUILDERS={
            "Cpp": SCons.Builder.Builder(
                action=SCons.Action.Action("$SCONSX_CPP_COM", "$SCONSX_CPP_COM_STR"),
                prefix="$SCONSX_CPP_PREFIX",
                suffix="$SCONSX_CPP_SUFFIX",
                src_suffix="$SCONSX_CPP_SRC_SUFFIX",
                single_source=True,
            ),
        },
    )

def exists(env):
    return env.WhereIs("cpp")
